# Générateur de feuilles de matchs de Kin-Ball

Script pour Google Sheets et "Apps Scripts"
Permet de générer des feuilles de matchs sportives pour du Kin-Ball.
Remplit automatiquement les infos générales (date, heure...), les joueurs, les numéros de licences, les coachs, les arbitres...

# Mode D'emploi 


Dans l'onglet "Liste des matchs à générer"  :				
	
- Remplir les champs Date, Lieu et Saison qui seront identiques dans toutes les feuilles de matchs			
- A partir de la ligne n°8, compléter les colonnes tableau. "Noms des Matchs" et "Terrain" seront accolés dans les feuilles de matchs dans la case "Identifiant". Chaque Nom doit être unique.			
Dans l'onglet "Liste joueurs-coachs"  :				
- Remplir les colonnes "Numéro Licence", "Joueur", "Equipe", "Coaching"			
- Si une case n'est pas complétée, pas d'inquiétude, cela ne sera juste pas pris en compte			
- Attention, l'équipe doit avoir exactement la même orthographe que dans la liste des matchs à générer : par exemple "Quintin F" est différent de "Quintin 1 F"			
L'onglet "Modèle de feuille de match" ne doit pas être modifié : si besoin de changer le format, le script devra être réadapté (voir plus loin).				
Une fois les matchs préparés, il suffit de cliquer sur le bouton vert "GENERER LES FEUILLES DE MATCHS" pour lancer la création des feuilles de matchs. 				
"La première fois, il va peut-être vous demander de valider des autorisations de modifications (4-5 écrans de validations). Une fois ces autorisations d'accès validées, vous pourrrez lancer  le script une seconde fois et cette fois-ci ça se lance.
Cela ne sera à faire que la première fois, ensuite le script se lancera directement."				
Le script se lance, comme l'indique une pop-up en haut de page. Les onglets se créent et s'auto-remplissent. Cela peut prendre jusqu'à une dizaine de secondes par match, ne pas perdre patience ;) 				
Une pop-up indiquera la fin du script. Ne rien modifier avant d'avoir cette pop-up.				
				
Les feuilles de matchs sont générées, une feuille par onglet.				
Pour l'impression ou la sauvegarde, il est possible de sélectionner tout le classeur, puis sélectionner uniquement les onglets générés, sans les onglets d'origine. 				
				
Il faut également selectionner "Ajuster à la page" pour que cela fasse bien une feuille de match par page. 				
				
Une fois terminé, n'oubliez pas de nettoyer la liste des matchs et de supprimer les onglets nouvellement créés pour ne pas surcharger le classeur.					
				
# Comment ça marche dans l'arrière-boutique ?				
				
Le script est largement commenté pour décrire le fonctionnement et permettre les modifications ultérieures si besoin. Même sans grande connaissance en dévellopement (n'en ayant pas beaucoup moi-même				
En cas de modification du modèle de la feuille de match, il y a un bloc noté "REMPLISSAGE" dans le script. C'est dans ce bloc que sont reporter les données de la liste dans chaque feuille de match. 				
Il y a également un bloc "REMPLISSAGE JOUEURS" : il est possible de le supprimer totalement sans impacter le fonctionnement du reste du script.				
La modification est relativement simple : il suffit de copier-coller une ligne de remplissage, renseigner la case à remplir et indiquer où se trouve l'info à y mettre. 				
En cas de tentative de modification du script, faites une sauvegarde du script d'origine au cas où vous casseriez tout ! ;)				
